(function () {
    var appName = 'Empresa';
    var appVersion = '1.0.0';
    var bootstrapVersion = '3.3.6';
    var angularVersion = '1.7.8';

    var breadcrumbPrefix = '';
    var breradcrumbPrefixName = '';

    var headTitle = 'Auranet - Portal Proveedores';

    /*
    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',
        'ngTouch',
        'pascalprecht.translate',
        'ncy-angular-breadcrumb',
        'ui.router',
        'ui.bootstrap',
        'angular-loading-bar',
        'xeditable',
        'toastr'
    ]);
    */

    angular
        .module('app',
            [
                'ngAnimate',
                'ngMessages',
                'ngSanitize',
                'ngTouch',
                'pascalprecht.translate',
                'ncy-angular-breadcrumb',
                'ui.router',
                'ui.bootstrap',
                'oc.lazyLoad',
                'xeditable',
                'toastr'
            ]
        );
})();