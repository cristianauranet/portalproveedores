(function () {
    angular
        .module('app')
        .config([
            '$translateProvider',
            '$stateProvider',
            '$urlRouterProvider',
            '$ocLazyLoadProvider',
            '$breadcrumbProvider',
            function (
                $translateProvider,
                $stateProvider,
                $urlRouterProvider,
                $ocLazyLoadProvider,
                $breadcrumbProvider
            ) {
                /* Ruteos de UI-Router */

                var homeState = {
                    name: 'home',
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'homeCtrl'
                };

                var loginState = {
                    name: 'login',
                    url: '/login',
                    templateUrl: 'views/login/login.html',
                    controller: 'loginCtrl'
                };

                var registerState = {
                    name: 'register',
                    url: '/register',
                    templateUrl: 'views/register/register.html',
                    controller: 'registerCtrl'
                };

                var searchState = {
                    name: 'search',
                    url: '/search',
                    templateUrl: 'views/search/search.html',
                    controller: 'searchCtrl'
                };

                var menuState = {
                    name: 'menu',
                    url: '/menu',
                    templateUrl: 'views/menu/menu.html',
                    controller: 'menuCtrl'
                };

                $stateProvider.state(homeState);
                $stateProvider.state(loginState);
                $stateProvider.state(registerState);
                $stateProvider.state(searchState);

                $urlRouterProvider.otherwise('/home');

                /* Angular Translate */

                $translateProvider.translations('es', {
                    'FOO': 'Hola, mundo!'
                });

                $translateProvider.translations('en', {
                    'FOO': 'Hello, world!'
                });

                $translateProvider.preferredLanguage('es');
                $translateProvider.useSanitizeValueStrategy('sanitize');

                /* OCLazyLoad */

                $ocLazyLoadProvider.config({
                    debug: true
                });
            }
        ]);
})();