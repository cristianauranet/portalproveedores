(function(){
    angular
        .module('app')
        .controller('menuCtrl', menuCtrl);

    menuCtrl.$inject = [
        '$scope',
        '$uibModal',
        '$window',
        '$timeout',
        'configConstant'
    ];

    function menuCtrl(
        $scope,
        $uibModal,
        $window,
        $timeout,
        configConstant
    ){
        /* Propiedades por defecto */

        var vm = this;
        vm.title = 'Vista de Menú Principal';

        // Es altamente probable que este menú no sea así.
        // Este controlador solamente se creó para señalar que hay una vista de Menu Principal
    }
})();