(function(){
    angular
        .module('app')
        .controller('registerCtrl', registerCtrl);

    registerCtrl.$inject = [
        '$scope',
        '$uibModal',
        '$window',
        '$timeout',
        '$location',
        'configConstant'
    ];

    function registerCtrl(
        $scope,
        $uibModal,
        $window,
        $timeout,
        $location,
        configConstant
    ){
        /* Propiedades por defecto */

        var vm = this;
        vm.title = 'Regístrese';

        vm.paso = 1;
        vm.tipoRegistro = 'empresa';

        vm.enableTooltip = function(){
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        };

        /* Inicialización */

        vm.enableTooltip();
    }
})();