(function () {
    angular
        .module('app')
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = [
        '$scope',
        '$uibModal',
        '$window',
        '$timeout',
        '$location',
        'configConstant'
    ];

    function homeCtrl(
        $scope,
        $uibModal,
        $window,
        $timeout,
        $location,
        configConstant
    ) {
        /* Propiedades por defecto */

        var vm = this;
        vm.title = 'Vista Home';

        vm.logged = false;
        vm.email = '';

        /* Propiedades para esta vista */

        vm.currentDate = new Date();
        vm.currentYear = vm.currentDate.getFullYear();

        vm.searchProveedorForm = {
            palabraClave: '',
            ubicacion: ''
        };

        vm.searchMaquinariaForm = {
            palabraClave: '',
            ubicacion: ''
        };

        vm.contactForm = {
            nombre: '',
            telefono: '',
            mail: '',
            rutEmpresa: '',
            mensaje: ''
        };

        vm.socialNetworks = [{
                title: 'LinkedIn',
                url: 'http://linkedin.com/example',
                icon: 'fa-linkedin'
            },
            {
                title: 'Instagram',
                url: 'http://instagram.com/example',
                icon: 'fa-instagram'
            },
            {
                title: 'Facebook',
                url: 'http://facebook.com/example',
                icon: 'fa-facebook-f'
            }
        ];

        vm.contact = {
            direccion: 'Padre Hurtado Sur 381',
            comuna: 'Las Condes',
            ciudad: 'Santiago de Chile',
            telefono: '+56 2 216 0070',
            email: 'contacto@portalauranet.cl'
        };

        vm.autocompleteDireccionProveedores = null;
        vm.autocompleteDireccionMaquinarias = null;

        /* Funciones */

        /* Funciones a nivel de Barra de Navegación */

        vm.openNuevoUsuarioModal = function () {
            $('#myNavbar').collapse('hide');

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'commons/templates/loginModal/loginModal.html',
                controller: 'loginModalCtrl',
                scope: $scope,
                size: 'md',
                resolve: {
                    payload: function () {
                        return {
                        };
                    }
                }
            });

            modalInstance.result.then(function (resolve) {
                console.log('home modal instance resolve');
                switch(resolve.action) {
                    case 'login': {
                        vm.processLogin(resolve);
                        break;
                    }

                    case 'register': {
                        $timeout(function(){
                            $location.path('/register');
                        }, 100);
                        break;
                    }

                    case 'resetPassword': {
                        $timeout(function(){
                            vm.openResetPasswordModal();
                        }, 200);
                        break;
                    }
                }
                console.log(resolve);
            }, function (reject) {
                console.log('home modal instance reject');
                console.log(reject);
            });
        };

        vm.openResetPasswordModal = function(){
            $('#myNavbar').collapse('hide');

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'commons/templates/resetPasswordModal/resetPasswordModal.html',
                controller: 'resetPasswordModalCtrl',
                scope: $scope,
                size: 'md',
                resolve: {
                    payload: function () {
                        return {};
                    }
                }
            });

            modalInstance.result.then(function (resolve) {
                console.log('reset password modal resolve');
                console.log(resolve);
            }, function (reject) {
                console.log('reset password modal reject');
                console.log(reject);
            });
        };

        vm.goToCarousel = function (item) {
            $('#myNavbar').collapse('hide');
            $('#homeCarousel').carousel(item);
        };

        vm.processLogin = function (obj) {
            console.log(vm.processLogin);
            vm.email = obj.loginObj.email;
            vm.logged = true;
        };

        vm.logout = function () {
            vm.logged = false;
            vm.email = '';
        };

        /* Funciones a nivel de Carrusel */

        vm.initAutocomplete = function () {
            var options = {
                componentRestrictions: {
                    country: 'cl'
                }
            };

            var fields = [
                'address_components', 'formatted_address', 'geometry', 'icon', 'name'
            ];

            vm.autocompleteDireccionProveedores = new google.maps.places.Autocomplete(document.getElementById('ubicacionProveedor'), options);
            vm.autocompleteDireccionMaquinarias = new google.maps.places.Autocomplete(document.getElementById('ubicacionMaquinaria'), options);

            vm.autocompleteDireccionProveedores.setFields(fields);
            vm.autocompleteDireccionMaquinarias.setFields(fields);

            vm.autocompleteDireccionProveedores.addListener('place_changed', vm.fillDireccionProveedores);
            vm.autocompleteDireccionMaquinarias.addListener('place_changed', vm.fillDireccionMaquinarias);
        };

        vm.fillDireccionProveedores = function () {
            var place = vm.autocompleteDireccionProveedores.getPlace();

            if (!place.geometry) {
                alert('No hay detalles para esta ubicación');
                return;
            }

            $scope.$apply(function () {
                vm.searchProveedorForm.ubicacion = place.formatted_address;
            }, 100);
        };

        vm.fillDireccionMaquinarias = function () {
            var place = vm.autocompleteDireccionMaquinarias.getPlace();

            if (!place.geometry) {
                alert('No hay detalles para esta ubicación');
                return;
            }

            $scope.$apply(function () {
                vm.searchMaquinariaForm.ubicacion = place.formatted_address;
            }, 100);
        };

        /* Funciones a nivel de Contacto */

        /* Funciones auxiliares */

        $('#contactOption').on('click', function (event) {
            $('#myNavbar').collapse('hide');

            if (this.hash !== '') {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 20
                }, 800);
            }
        });

        /* Inicialización */

        vm.initAutocomplete();

    }
})();