(function(){
    angular
        .module('app')
        .controller('searchCtrl', searchCtrl);

    searchCtrl.$inject = [
        '$scope',
        '$uibModal',
        '$window',
        '$timeout',
        '$location',
        'configConstant'
    ];

    function searchCtrl(
        $scope,
        $uibModal,
        $window,
        $timeout,
        $location,
        configConstant
    ){
        /* Propiedades por defecto */

        var vm = this;
        vm.title = "Vista de Búsquedas";

        vm.logged = false;
        vm.email = '';

        console.log(vm);

        /* Propiedades para esta vista */

        vm.buscarProveedores = {

        };

        vm.buscarMaquinarias = {

        };

        /* Funciones */

        /* Funciones a nivel de Barra de Navegación */

        vm.openNuevoUsuarioModal = function(){
            $('#myNavbar').collapse('hide');

            var modalInstance = $uibModal.open({
                animation : true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'commons/templates/loginModal/loginModal.html',
                controller: 'loginModalCtrl',
                scope: $scope,
                size: 'md',
                resolve: {
                    payload: function(){
                        return {};
                    }
                }
            });

            modalInstance.result.then(function(resolve){
                console.log('search modal instance resolve');

                console.log(resolve);

                switch(resolve.action) {
                    case 'login': {
                        vm.processLogin(resolve);
                        break;
                    }

                    case 'register': {
                        $timeout(function(){
                            $location.path('/register');
                        }, 100);
                        break;
                    }

                    case 'resetPassword': {
                        $timeout(function(){
                            vm.openResetPasswordModal();
                        }, 200);
                        break;
                    }
                }

                console.log(resolve);
                // vm.email = resolve.loginObj.email;
                // vm.logged = true;
            }, function(reject){
                console.log(reject);
            });
        };

        vm.openResetPasswordModal = function(){
            console.log('open reset password modal');
            $('#myNavbar').collapse('hide');

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'commons/templates/resetPasswordModal/resetPasswordModal.html',
                controller: 'resetPasswordModalCtrl',
                scope: $scope,
                size: 'md',
                resolve: {
                    payload: function () {
                        return {};
                    }
                }
            });

            modalInstance.result.then(function (resolve) {
                console.log('reset password modal resolve');
                console.log(resolve);
            }, function (reject) {
                console.log('reset password modal reject');
                console.log(reject);
            });
        };

        vm.processLogin = function(obj){
            console.log('vm.processLogin()');
            vm.email = obj.loginObj.email;
            vm.logged = true;
            console.log(vm.logged);
        };

        vm.logout = function () {
            vm.logged = false;
            vm.email = '';
        };
    }
})();