(function(){
    angular
        .module('app')
        .controller('loginModalCtrl', loginModalCtrl);

    loginModalCtrl.$inject = [
        '$scope',
        'payload',
        '$uibModalInstance'
    ];

    function loginModalCtrl($scope, payload, $uibModalInstance){
        /* Propiedades para el modal */

        $scope.empresaLogin = {
            idFiscal: '',
            email: '',
            pass: ''
        };

        $scope.personaLogin = {
            email: '',
            pass: ''
        };

        $scope.empresaUserTest = {
            idFiscal: '12345789-0',
            email: 'cristian@auranet.cl',
            pass: '1234'
        };

        $scope.personaUserTest = {
            email: 'cristian@auranet.cl',
            pass: '1234'
        };

        $scope.invalidLogin = false;

        /* Funciones */

        $scope.processEmpresaLogin = function(){
            console.log('processEmpresaLogin()');

            if ($scope.empresaUserTest.idFiscal == $scope.empresaLogin.idFiscal && $scope.empresaUserTest.email == $scope.empresaLogin.email && $scope.empresaUserTest.pass == $scope.empresaLogin.pass) {
                var loginInfo = {
                    loginObj: $scope.empresaLogin,
                    tipo: 'empresa',
                    action: 'login'
                };

                $uibModalInstance.close(loginInfo);
            } else {
                $scope.invalidLogin = true;
            }
        };

        $scope.processPersonaLogin = function(){
            console.log('processPersonaLogin()');

            if ($scope.personaUserTest.email == $scope.personaLogin.email && $scope.personaUserTest.pass == $scope.personaLogin.pass) {
                var loginInfo = {
                    loginObj: $scope.personaLogin,
                    tipo: 'persona',
                    action: 'login'
                };

                $uibModalInstance.close(loginInfo);
            } else {
                $scope.invalidLogin = true;
            }
        };

        $scope.connectFacebook = function(){
            console.log('connectFacebook()');

            var loginInfo = {
                action: 'login'
            };

            $uibModalInstance.close(loginInfo);
        };

        $scope.connectGoogle = function(){
            console.log('connectGoogle()');

            var loginInfo = {
                action: 'login'
            };

            $uibModalInstance.close(loginInfo);
        };

        $scope.registerUser = function(){
            var loginInfo = {
                action: 'register'
            };

            $uibModalInstance.close(loginInfo);
        };

        $scope.resetPassword = function(){
            var loginInfo = {
                action: 'resetPassword'
            };

            $uibModalInstance.close(loginInfo);
        };
    }
})();