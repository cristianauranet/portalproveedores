(function(){
    angular
        .module('app')
        .controller('resetPasswordModalCtrl', resetPasswordModalCtrl);

    resetPasswordModalCtrl.$inject = [
        '$scope',
        'payload',
        '$uibModalInstance'
    ];

    function resetPasswordModalCtrl($scope, payload, $uibModalInstance){
        /* Propiedades para el modal */

        console.log(payload);

        $scope.empresaReset = {

        };

        $scope.personaReset = {

        };

        /* Funciones */

        $scope.processResetPasswordEmpresa = function(){
            $uibModalInstance.close({});
        };

        $scope.processResetPasswordPersona = function(){
            $uibModalInstance.close({});
        };
    }
})();